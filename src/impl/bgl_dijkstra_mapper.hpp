#include "mapper.hpp"
#include "utility.hpp"

#include <iostream>
#include <cmath>

#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/labeled_graph.hpp>
#include <boost/graph/graph_utility.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/graphviz.hpp>


namespace routing {

using namespace boost;

using Graph = adjacency_list<listS, vecS, undirectedS,
                             property<vertex_name_t, Node::Id>,
                             property<edge_weight_t, Node::Accumulation>>;
using LabeledGraph = labeled_graph<Graph, Node::Id>;
using VertexDescriptor = graph_traits<LabeledGraph>::vertex_descriptor;

namespace {

Node::Accumulation maxEdgeWeight(const NetworkMap &network) {
    auto maximum = Node::Accumulation{ 0 };

    for (const auto &c : network.connections) {
        auto s_iter = network.nodes.find(c.source);
        auto t_iter = network.nodes.find(c.target);

        if (s_iter == network.nodes.end() || t_iter == network.nodes.end()) {
            continue;
        }

        maximum = std::max(maximum, s_iter->second + t_iter->second);
    }

    return maximum;
}

Node::Accumulation edgeWeight(const Node::Id &source, const Node::Id &target,
                              const NetworkMap &network) {
    return network.nodes.at(source) + network.nodes.at(target);
}

LabeledGraph buildGraph(const NetworkMap &network) {
    LabeledGraph result;

    for (const auto &node : network.nodes) {
        const auto id = node.first;
        add_vertex(id, id, result);
    }

    const auto maxWeight = maxEdgeWeight(network) + 1;

    for (const auto &connection : network.connections) {
        const auto sourceId = connection.source;
        const auto targetId = connection.target;

        auto sourceVertex = vertex_by_label(sourceId, result);
        if (sourceVertex == routing::LabeledGraph::null_vertex()) {
            std::cerr << "Source with id=" << sourceId << " does not exist.\n";
            continue;
        }

        auto targetVertex = vertex_by_label(targetId, result);
        if (targetVertex == routing::LabeledGraph::null_vertex()) {
            std::cerr << "Target with id=" << targetId << " does not exist.\n";
            continue;
        }

        if (!edge(sourceVertex, targetVertex, result).second) {
            const auto weight = edgeWeight(sourceId, targetId, network);
            const auto inversedWeight = (std::abs(weight - maxWeight) + 1);
            add_edge(sourceVertex, targetVertex, inversedWeight, result);
        }
    }

    return result;
}

}

class Mapper::Impl {
    struct TargetFound {};

    class EarlyExitVisitor : public default_dijkstra_visitor {
    public:
        explicit EarlyExitVisitor(VertexDescriptor target) :target_(target) {}

        void discover_vertex(VertexDescriptor vertex,
                             const LabeledGraph &graph) const {
            if (vertex == target_) {
                throw TargetFound();
            }
        }

    private:
        VertexDescriptor target_;
    };

public:
    const std::optional<NetworkMap> &network() noexcept {
        return network_;
    }

    void update(const NetworkMap &network) {
        network_ = utility::sanitize(network);
        graph_ = buildGraph(*network_);
    }

    std::optional<Node::Id> bestPathNext(const Node::Id &source,
                                         const Node::Id &target,
                                         std::error_code &ec) {
        auto path = bestPathFull(source, target, ec);
        if (source == target) return source;
        if (path) return path->at(1);
        return std::nullopt;
    }

    std::optional<std::vector<Node::Id>> bestPathFull(const Node::Id &source,
                                                      const Node::Id &target,
                                                      std::error_code &ec) {
        if (!network_) {
            ec = make_error_code(routing_errc::no_routing_table);
            return std::nullopt;
        }

        auto sourceVertex = vertex_by_label(source, graph_);
        auto targetVertex = vertex_by_label(target, graph_);

        if (sourceVertex == routing::LabeledGraph::null_vertex()) {
            ec = make_error_code(routing_errc::source_not_found);
            return std::nullopt;
        }

        if (targetVertex == routing::LabeledGraph::null_vertex()) {
            ec = make_error_code(routing_errc::target_not_found);
            return std::nullopt;
        }

        if (source == target) {
            return std::vector<Node::Id>{source};
        }

        std::vector<VertexDescriptor> predecessorMap(num_vertices(graph_));
        std::vector<Node::Accumulation> distanceMap(num_vertices(graph_));

        try {
            EarlyExitVisitor visitor(targetVertex);
            auto im = get(vertex_index, graph_);
            auto p = make_iterator_property_map(predecessorMap.begin(), im);
            auto d = make_iterator_property_map(distanceMap.begin(), im);
            auto params = predecessor_map(p).distance_map(d).visitor(visitor);
            dijkstra_shortest_paths(graph_, sourceVertex, params);

            ec = make_error_code(routing_errc::no_route);
            return std::nullopt;

        } catch (TargetFound &e) {
            std::vector<VertexDescriptor> path;

            auto current = targetVertex;
            while (current != sourceVertex) {
                path.push_back(current);
                current = predecessorMap[current];
            }
            path.push_back(sourceVertex);

            std::vector<Node::Id> result;
            for (auto it = path.rbegin(); it != path.rend(); ++it) {
                result.push_back(get(vertex_name, graph_, *it));
            }
            return result;
        }

        return std::nullopt;
    }

private:
    std::optional<NetworkMap> network_;
    LabeledGraph graph_;
};

}
