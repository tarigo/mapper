#include "mapper.hpp"
#include "json.hpp"

#include "impl/bgl_dijkstra_mapper.hpp"

namespace routing {
using nlohmann::json;

Mapper::Mapper() : pimpl_{std::make_unique<Impl>()} {}

Mapper::~Mapper() = default;

const std::optional<NetworkMap> &Mapper::network() noexcept {
    return pimpl_->network();
}

void Mapper::update(const NetworkMap &network) {
    pimpl_->update(network);
}

std::optional<Node::Id> Mapper::bestPathNext(const Node::Id &source,
                                             const Node::Id &target,
                                             std::error_code &ec) {
    return pimpl_->bestPathNext(source, target, ec);
}

std::optional<std::vector<Node::Id>>
Mapper::bestPathFull(const Node::Id &source,
                     const Node::Id &target,
                     std::error_code &ec) {
    return pimpl_->bestPathFull(source, target, ec);
}
}

