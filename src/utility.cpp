#include "mapper.hpp"
#include "json.hpp"

#include <iostream>

namespace routing {
  using nlohmann::json;

  void to_json(json &j, const Node &n) {
    j = json {
              { "id", n.id },
              { "accumulation", n.accumulation },
    };
  }

  void from_json(const json &j, Node &n) {
    j.at("id").get_to(n.id);
    j.at("accumulation").get_to(n.accumulation);
  }

  void to_json(json &j, const NetworkMap::Connection &c) {
    j = json {
              { "source", c.source },
              { "target", c.target }
    };
  }

  void from_json(const json &j, NetworkMap::Connection &c) {
    j.at("source").get_to(c.source);
    j.at("target").get_to(c.target);
  }

  void to_json(json &j, const NetworkMap &t) {
    j["nodes"] = t.nodes;
    j["connections"] = t.connections;
  }

  void from_json(const json &j, NetworkMap &t) {
    j.at("nodes").get_to(t.nodes);
    j.at("connections").get_to(t.connections);
  }

  std::ostream& operator << (std::ostream& os, const NetworkMap::Connection &c) {
    os << "[" << c.source << ", " << c.target << "]";
    return os;
  }
}

namespace routing::utility {
  /**
   * @brief Unmarshalling JSON.
   */
  NetworkMap deserialize(const std::string &json_string) {
    NetworkMap n = json::parse(json_string);
    return n;
  }

  /**
   * @brief Marshalling to JSON.
   */
  std::string serialize(const NetworkMap &network) {
    json j = network;
    return j.dump();
  }

  NetworkMap::Nodes sanitize(const NetworkMap::Nodes &nodes) {
    NetworkMap::Nodes result;
    for (const auto &n : nodes) {
      const auto id = n.first;
      if (id.empty()) continue;
      if (result.count(id)) continue;
      result[id] = n.second;
    }
    return result;
  }

  NetworkMap::Connections sanitize(const NetworkMap::Nodes &nodes,
                                   const NetworkMap::Connections &connections) {
    NetworkMap::Connections result;

    for (const auto &c : connections) {
      if (c.source.empty() || c.target.empty()) {
        std::cerr << "Connection contains invalid id: " << c << std::endl;
        continue;
      }

      if (c.source == c.target) {
        std::cerr << "Connection is loop: " << c << std::endl;
        continue;
      }

      auto s_iter = nodes.find(c.source);
      auto t_iter = nodes.find(c.target);

      if (s_iter == nodes.end() || t_iter == nodes.end()) {
        std::cerr << "Connection contains stray node(s): " << c << std::endl;
        continue;
      }

      result.push_back({s_iter->first, t_iter->first});
    }

    return result;
  }
}
