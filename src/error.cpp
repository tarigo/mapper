#include "error.hpp"

namespace routing {
  const char *error_category_impl::name() const noexcept {
    return "routing";
  }

  std::string error_category_impl::message(int ev) const {
    if (ev == static_cast<int>(routing_errc::no_routing_table))
      return "routing table is empty";
    if (ev == static_cast<int>(routing_errc::source_not_found))
      return "cannot find source in the routing table";
    if (ev == static_cast<int>(routing_errc::target_not_found))
      return "cannot find target in the routing table";
    if (ev == static_cast<int>(routing_errc::no_route))
      return "no route";
    return "unknown error";
  }

  bool error_category_impl::equivalent(const std::error_code &code,
                                       int condition) const noexcept {
    return false;
  }

  const std::error_category &error_category() {
    static error_category_impl instance;
    return instance;
  }

  std::error_code make_error_code(routing::routing_errc e) {
    return std::error_code(static_cast<int>(e), routing::error_category());
  }

  std::error_condition
  error_category_impl::default_error_condition(int ev) const noexcept {
    switch (ev) {
    default:
      return std::error_condition(ev, *this);
    }
  }
}
