#pragma once

#include "mapper.hpp"

namespace routing::utility {

/**
 * @brief Unmarshalling from JSON.
 */
NetworkMap deserialize(const std::string &json_string);

/**
 * @brief Marshalling to JSON.
 */
std::string serialize(const NetworkMap &network);

/**
 * @brief Remove nodes with empty IDs, deduplicate.
 */
NetworkMap::Nodes sanitize(const NetworkMap::Nodes &nodes);

/**
 * @brief Remove loops, connections between stray nodes and empty IDs.
 */
NetworkMap::Connections sanitize(const NetworkMap::Nodes &nodes,
                                 const std::vector<NetworkMap::Connection> &connections);


static inline NetworkMap sanitize(const NetworkMap &network) {
    NetworkMap result { sanitize(network.nodes), {} };
    result.connections = sanitize(result.nodes, network.connections);
    return result;
}

}