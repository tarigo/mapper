#pragma once

#include <memory>
#include <string>
#include <vector>
#include <map>
#include <optional>
#include <cstdint>

#include "error.hpp"

namespace routing {
/**
 * @brief Structure to represent network communication endpoint.
 */
struct Node {
    using Id = std::string;
    using Accumulation= int;

    Id id;
    Accumulation accumulation;
};

/**
 * @brief Structure to represent network.
 */
struct NetworkMap {
    struct Connection {
        Node::Id source;
        Node::Id target;
    };

    using Nodes = std::map<Node::Id, Node::Accumulation>;
    using Connections = std::vector<Connection>;

    Nodes nodes;
    Connections connections;
};

/**
 * @brief Class to operate on routing table.
 */
class Mapper {
public:
    Mapper();

    virtual ~Mapper();

    const std::optional<NetworkMap> &network() noexcept;

    void update(const NetworkMap &network);

    std::optional<Node::Id> bestPathNext(const Node::Id &source,
                                         const Node::Id &target,
                                         std::error_code &ec);

    std::optional<std::vector<Node::Id>> bestPathFull(const Node::Id &source,
                                                      const Node::Id &target,
                                                      std::error_code &ec);

private:
    class Impl;
    std::unique_ptr<Impl> pimpl_;
};

} //namespace routing
