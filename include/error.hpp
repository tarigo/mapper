#pragma once

#include <system_error>

namespace routing {

enum class routing_errc {
    no_routing_table,
    source_not_found,
    target_not_found,
    no_route
};

class error_category_impl : public std::error_category {
public:
    virtual const char *name() const noexcept;

    virtual std::string message(int ev) const;

    virtual bool equivalent(const std::error_code &code,
                            int condition) const noexcept;

    virtual std::error_condition default_error_condition(int e) const noexcept;
};

const std::error_category &error_category();

std::error_code make_error_code(routing::routing_errc e);
}

namespace std {
template<>
struct is_error_code_enum<routing::routing_errc> : public true_type {
};

} //namespace routing
