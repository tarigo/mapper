#include "catch.hpp"

#include "mapper.hpp"

#include <iostream>

using namespace routing;

const auto wellFormedNetwork = NetworkMap {
        {
                { "a", 1 },
                { "b", 4 },
                { "c", 3 },
                { "d", 3 },
                { "e", 3 },
                { "f", 5 },
                { "g", 8 },
                { "h", 1 }
        },
        {
                {"a", "b"},

                {"b", "c"},
                {"b", "d"},
                {"b", "f"},

                {"c", "b"},
                {"c", "e"},

                {"d", "b"},
                {"d", "e"},

                {"e", "f"},
                {"e", "g"},

                {"f", "e"},
                {"f", "g"},
                {"f", "h"},

                {"g", "e"},
                {"g", "f"},
                {"g", "h"},

                {"h", "f"},
                {"h", "g"}
        }
};

TEST_CASE("Check network map after creation", "[mapper]") {
  auto mapper = Mapper();
  REQUIRE(!mapper.network());
}

TEST_CASE("Try to route on empty table", "[mapper]") {
  auto mapper = Mapper();
  std::error_code ec;
  auto node_id = mapper.bestPathNext("a", "b", ec);
  REQUIRE(!node_id);
  REQUIRE(ec == routing_errc::no_routing_table);
}

TEST_CASE("Full path on well-formed network", "[mapper]") {
  auto mapper = Mapper();

  mapper.update(wellFormedNetwork);
  std::error_code ec;
  auto path = mapper.bestPathFull("h", "a", ec);

  auto expectedPath = std::vector<Node::Id> { "h", "g", "f", "b", "a" };

  REQUIRE(path);
  REQUIRE(path->size() == expectedPath.size());
  REQUIRE(std::equal(expectedPath.begin(), expectedPath.end(), path->begin()));

}


TEST_CASE("Next on well-formed network", "[mapper]") {
  auto mapper = Mapper();

  mapper.update(wellFormedNetwork);
  std::error_code ec;
  auto next = mapper.bestPathNext("h", "a", ec);

  REQUIRE(next);
  REQUIRE(*next == "g");
}

TEST_CASE("Source and target are the same", "[mapper]") {
  auto mapper = Mapper();

  mapper.update(wellFormedNetwork);
  std::error_code ec;
  auto next = mapper.bestPathNext("a", "a", ec);

  REQUIRE(next);
  REQUIRE(*next == "a");
}

TEST_CASE("Source does not exists", "[mapper]") {
  auto mapper = Mapper();

  mapper.update(wellFormedNetwork);
  std::error_code ec;
  auto next = mapper.bestPathNext("x", "a", ec);

  REQUIRE(!next);
  REQUIRE(ec == routing_errc::source_not_found);
}

TEST_CASE("Target does not exists", "[mapper]") {
  auto mapper = Mapper();

  mapper.update(wellFormedNetwork);
  std::error_code ec;
  auto next = mapper.bestPathNext("a", "x", ec);

  REQUIRE(!next);
  REQUIRE(ec == routing_errc::target_not_found);
}

TEST_CASE("No route", "[mapper]") {
  const auto hDisconnected = NetworkMap {
          {
                  { "a", 1 },
                  { "b", 4 },
                  { "c", 3 },
                  { "d", 3 },
                  { "e", 3 },
                  { "f", 5 },
                  { "g", 8 },
                  { "h", 1 }
          },
          {
                  {"a", "b"},

                  {"b", "c"},
                  {"b", "d"},
                  {"b", "f"},

                  {"c", "b"},
                  {"c", "e"},

                  {"d", "b"},
                  {"d", "e"},

                  {"e", "f"},
                  {"e", "g"},

                  {"f", "e"},
                  {"f", "g"},

                  {"g", "e"},
                  {"g", "f"},

          }
  };

  auto mapper = Mapper();
  mapper.update(hDisconnected);
  std::error_code ec;

  auto next = mapper.bestPathNext("h", "a", ec);
  REQUIRE(!next);
  REQUIRE(ec == routing_errc::no_route);

  next = mapper.bestPathNext("a", "h", ec);
  REQUIRE(!next);
  REQUIRE(ec == routing_errc::no_route);
}

TEST_CASE("Verify update()", "[mapper]") {
  auto mapper = Mapper();

  mapper.update(wellFormedNetwork);
  std::error_code ec;
  auto next = mapper.bestPathNext("h", "a", ec);
  REQUIRE(next);
  REQUIRE(*next == "g");

  const auto xyz = NetworkMap {
          {
                  { "x", 1 },
                  { "y", 1 },
                  { "z", 1 }
          },
          {
                  {"x", "y"},
                  {"y", "z"}
          }
  };

  mapper.update(xyz);

  next = mapper.bestPathNext("a", "h", ec);
  REQUIRE(!next);
  REQUIRE(ec == routing_errc::source_not_found);

  next = mapper.bestPathNext("x", "z", ec);
  REQUIRE(next);
  REQUIRE(*next == "y");
}
