#include "catch.hpp"
#include "utility.hpp"

#include <iostream>

using namespace routing;

TEST_CASE("Des/serialize", "[utility]") {
    auto network = NetworkMap{
            {
                    {"a", 1},
                    {"b", 2},
                    {"c", 1},
            },
            {
                    {"a", "b"},
                    {"b", "c"},
            }
    };

    auto str = utility::serialize(network);
    std::cout << str << std::endl;
    auto net = utility::deserialize(str);

    REQUIRE(network.nodes.size() == net.nodes.size());
    REQUIRE(network.connections.size() == net.connections.size());
}

TEST_CASE("Sanitize") {
    auto network = NetworkMap{
            {
                    {"a", 1}, // valid
                    {"b", 2}, // valid
                    {"a", 1}, // duplicate
                    {"",  0}   // empty
            },
            {
                    {"a", "b"}, // valid
                    {"b", "c"}, // stray, "c"" does not exist
                    {"x", "z"}, // straym, both do not exist
                    {"a", "a"}, // loop
                    {"", "a"}  // invalid/empty id
            }
    };

    auto nodes = utility::sanitize(network.nodes);
    REQUIRE(nodes.size() == 2);
    auto connections = utility::sanitize(nodes, network.connections);
    REQUIRE(connections.size() == 1);

    auto sanitizedNetwork = utility::sanitize(network);
    REQUIRE(sanitizedNetwork.nodes.size() == 2);
    REQUIRE(sanitizedNetwork.connections.size() == 1);
}
